class Employee {
  constructor(name, age, salary) {
    this.name = name
    this.age = age
    this.salary = salary
  }

  get nameInfo() {
    return this.name

  }
  set nameInfo(newName) {
    this.name = newName
  }
  get ageInfo() {
    return this.age

  }
  set ageInfo(value) {
    this.age = value
  }
  get salaryInfo() {
    return this.salary

  }
  set salaryInfo(value) {
    this.salary = value
  }
}

class Programmer extends Employee {
  constructor (name, age, salary, lang) {
    super(name, age, salary)
    this.lang = lang
  }
  get salaryInfo() {
    return super.salaryInfo * 3

  }
}

const programmer = new Programmer('Olya', 23, 1200);
const programmer2 = new Programmer('Oleg', 32, 1300)
console.log(programmer)
console.log(programmer2)
console.log(programmer.salaryInfo)
